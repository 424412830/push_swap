/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/08 15:56:18 by abinet            #+#    #+#             */
/*   Updated: 2022/11/23 22:01:19 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stddef.h>
#include <stdio.h>
#include <bsd/string.h>
#include <stdlib.h>

size_t	ft_strlcpy(char *dst, const char *src, size_t size)
{
	unsigned int	i;

	i = 0;
	if (size == 0)
		return (ft_strlen(src));
	while (i < (size - 1) && src[i])
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (ft_strlen(src));
}
/*
int	main(int argc, char **argv)
{
	char	dst[15];
	char	dest[15];

	(void) argc;
	ft_strlcpy(dst,argv[1],atoi(argv[2]));
	strlcpy(dest,argv[1],atoi(argv[2]));
	printf("ft_strlcpy dst = %s\n", dst);
	printf("strlcpy dest = %s\n", dest);
	printf("ft_strlcpy return = %zu\n", ft_strlcpy(dst,argv[1],atoi(argv[2])));
	printf("strlcpy return = %zu\n", strlcpy(dest,argv[1],atoi(argv[2])));
	return (0);
}
*/
