/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/18 10:30:35 by abinet            #+#    #+#             */
/*   Updated: 2022/12/01 14:10:08 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stdlib.h>

static int	cnt_words(char const *s, char c)
{
	int	i;
	int	cnt;

	i = 0;
	cnt = 0;
	while (s[i])
	{
		if (s[i] != c && (s[i + 1] == c || s[i + 1] == '\0'))
			cnt++;
		i++;
	}
	return (cnt);
}

static int	strlen_nextc(const char *s, char c)
{
	int	i;

	i = 0;
	while (s[i] != c && s[i] != '\0')
		i++;
	return (i);
}

static void	free_tab(char **res)
{
	int	i;

	i = 0;
	while (res[i])
	{
		free(res[i]);
		i++;
	}
}

static char	**fill_tab(char const *s, char c, char **res)
{
	unsigned int	i;
	int				j;
	int				k;

	i = 0;
	k = 0;
	while (i < ft_strlen(s) && cnt_words((s), c) > k)
	{
		while (s[i] == c)
			i++;
		res[k] = malloc(sizeof(char) * (strlen_nextc(&s[i], c) + 1));
		if (!res[k])
		{
			free_tab(res);
			return (NULL);
		}
		j = 0;
		while (s[i] != c && i < ft_strlen(s))
			res[k][j++] = s[i++];
		res[k][j] = '\0';
		k++;
	}
	res[k] = NULL;
	return (res);
}

char	**ft_split(char const *s, char c)
{
	char	**res;

	if (!s)
		return (NULL);
	res = malloc(sizeof(char *) * (cnt_words(s, c) + 1));
	if (!res)
		return (NULL);
	fill_tab(s, c, res);
	return (res);
}
/*
int	main(int argc, char **argv)
{
	char	**res;
	int		i;

	(void) argc;
	(void) argv;
	i = 0;
	res = ft_split(argv[1], argv[2][0]);

	printf("%d\n", strlen_nextc(argv[1], argv[2][0]));

	while (res[i])
	{
		printf("%s ", res[i]);
		i++;
	}
	return (0);
}
*/
