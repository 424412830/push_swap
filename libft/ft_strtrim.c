/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/17 11:43:49 by abinet            #+#    #+#             */
/*   Updated: 2022/12/01 12:39:40 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stdlib.h>

static int	is_trim(char c, const char *set)
{
	while (*set)
	{
		if (*set == c)
			return (1);
		set++;
	}
	return (0);
}

static int	len_without_trim(const char *s1, const char *set)
{
	int	i;
	int	j;
	int	len;

	len = ft_strlen(s1);
	i = 0;
	j = len - 1;
	while (is_trim(s1[i], set) == 1)
		i++;
	if (len > i)
	{
		while (is_trim(s1[j], set) == 1)
		{
			i++;
			j--;
		}
	}
	return (len - i);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*dest;
	size_t	i;
	size_t	j;
	size_t	l;

	if (!s1)
		return (NULL);
	i = 0;
	j = 0;
	l = len_without_trim(s1, set);
	dest = malloc(sizeof(char) * (l + 1));
	if (!dest)
		return (NULL);
	while (is_trim(s1[i], set) == 1)
		i++;
	while (j < l)
	{
			dest[j] = s1[i];
			i++;
			j++;
	}
	dest[j] = '\0';
	return (dest);
}
/*
int	main(int argc, char **argv)
{
	char	*test;

	(void) argc;
	test = ft_strtrim(argv[1], argv[2]);
	printf("%s\n", test);
	//printf("%d\n", len_without_trim(argv[1], argv[2]));
	//printf("%ld\n", ft_strlen(test));
	return (0);
}
*/
