/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/11 19:52:34 by abinet            #+#    #+#             */
/*   Updated: 2022/12/01 11:20:42 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <bsd/string.h>
#include <stdlib.h>

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	i;
	size_t	j;

	if (len == 0 && !big)
		return (NULL);
	i = -1;
	if (*little == '\0')
		return ((char *)big);
	while (big[++i] && i < len)
	{
		j = 0;
		while (big[i] == little[j] && big[i] && i < len)
		{
			i++;
			j++;
		}
		if (little[j] == '\0')
			return ((char *)&big[i - j]);
		i = i - j;
	}
	return (NULL);
}
/*
int	main(int argc, char **argv)
{
	char	*res;
	char *result;

	(void) argc;
	res = ft_strnstr(argv[1], argv[2], atoi(argv[3]));
	result = strnstr(argv[1], argv[2], atoi(argv[3]));
	printf("%p\n%s\n", res, res);
	printf("%p\n%s\n", result, result);
	return (0);
}
*/
