/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/16 14:44:22 by abinet            #+#    #+#             */
/*   Updated: 2022/11/30 16:22:28 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	*ft_memmove(void *dest, const void *src, size_t size)
{
	char	*src2;
	char	*dest2;

	if (!src && !dest)
		return (NULL);
	src2 = (char *)src;
	dest2 = (char *)dest;
	if (dest > src)
	{
		while (size--)
			dest2[size] = src2[size];
	}
	else
		ft_memcpy(dest, src, size);
	return (dest);
}
/*
int	main(int argc, char **argv)
{
	char	*dest;

	(void)argc;
	dest = malloc(sizeof(char) * 10);
	if (!dest)
		return (0);
	ft_memmove(dest, argv[1], atoi(argv[2]));
	printf("ft_memmove = %p\n", dest);
	memmove(dest, argv[1], atoi(argv[2]));
	printf("   memmove = %p\n", dest);
	free(dest);
	return (0);
}
*/
