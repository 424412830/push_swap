/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/14 18:01:25 by abinet            #+#    #+#             */
/*   Updated: 2022/12/03 10:28:26 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <limits.h>

void	*ft_calloc(size_t nmemb, size_t size)
{
	void	*res;

	if (size != 0 && nmemb > SIZE_MAX / size)
		return (NULL);
	res = malloc(size * nmemb);
	if (!res)
		return (NULL);
	ft_bzero(res, size * nmemb);
	return (res);
}
/*
int	main(void)
{
	long	*buffer;

	buffer = (long *)ft_calloc(40, 0);
	if (buffer != NULL)
		printf("Allocated 40 long integers\n");
	else
		printf("Can't allocate memory\n");
	free(buffer);
}
*/
