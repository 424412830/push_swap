/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_lst.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/24 15:32:41 by abinet            #+#    #+#             */
/*   Updated: 2023/03/08 14:52:40 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

t_stack	*ft_lstnew_t(int value)
{
	t_stack	*new;

	new = malloc(sizeof(t_stack));
	if (!new)
		return (NULL);
	new->value = value;
	new->order = 0;
	new->next = NULL;
	return (new);
}

void	ft_lstadd_front_t(t_stack **stack, t_stack *new)
{
	if (!(*stack))
	{
		*stack = new;
		return ;
	}
	if (!new)
		return ;
	new->next = *stack;
	*stack = new;
}

void	ft_lstclear_t(t_stack **stack)
{
	t_stack	*temp;

	if (!(stack) || !(*stack))
		return ;
	while ((*stack))
	{
		temp = (*stack)->next;
		free(*stack);
		*stack = temp;
	}
	stack = NULL;
}

int	ft_lstsize_t(t_stack *stack)
{
	int	i;

	i = 0;
	while (stack != NULL)
	{
		stack = stack->next;
		i++;
	}
	return (i);
}

t_stack	*ft_lstlast_t(t_stack *stack)
{
	if (!stack)
		return (NULL);
	while (stack->next)
		stack = stack->next;
	return (stack);
}
