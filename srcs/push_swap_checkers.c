/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_checkers.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/24 15:36:52 by abinet            #+#    #+#             */
/*   Updated: 2023/03/20 11:10:01 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

int	check_args(char	*str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if ((str[i] < 48 || str[i] > 57) && str[i] != '-')
			return (1);
		i++;
	}
	if (i == 0 || i > 11)
		return (1);
	if (i == 10 || i == 11)
	{
		if (ft_atoi_long(str) > 2147483647 || ft_atoi_long(str) < -2147483648)
			return (1);
	}
	return (0);
}

int	check_same_nbr(t_stack *stack)
{
	t_stack	*current;

	if (!stack)
		return (0);
	while (stack)
	{
		current = stack->next;
		while (current)
		{
			if (stack->value == current->value)
				return (1);
			current = current->next;
		}
		stack = stack->next;
	}
	return (0);
}

int	checker_order(t_stack *stack)
{
	int	min;
	int	max;

	min = find_min(stack);
	max = find_max(stack);
	if (!stack)
		return (0);
	while ((stack)->next)
	{
		if ((stack)->order >= ((stack)->next->order))
			return (1);
		stack = (stack)->next;
	}
	return (0);
}

int	checker_inverse_order(t_stack *stack)
{
	int	min;
	int	max;

	min = find_min(stack);
	max = find_max(stack);
	if (!stack)
		return (0);
	while ((stack)->next)
	{
		if ((stack)->order <= ((stack)->next->order))
			return (1);
		stack = (stack)->next;
	}
	return (0);
}
