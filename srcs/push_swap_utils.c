/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 17:52:35 by abinet            #+#    #+#             */
/*   Updated: 2023/03/20 15:44:45 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

// void	print_stack(t_stack *stack_a, t_stack *stack_b)
// {
// 	int	size_a;
// 	int	size_b;
// 	int	diff;

// 	size_a = ft_lstsize_t(stack_a);
// 	size_b = ft_lstsize_t(stack_b);
// 	ft_printf("\n");
// 	ft_printf("	value	order	value	order\n");
// 	ft_printf("	------	------	-----	------\n");
// 	while ((stack_a || stack_b) && (size_a || size_b))
// 	{
// 		diff = size_a - size_b;
// 		if (stack_a && diff >= 0)
// 		{
// 			ft_printf("	%d	%d", stack_a->value, stack_a->order);
// 			stack_a = stack_a->next;
// 			size_a--;
// 		}
// 		else if (diff < 0)
// 			ft_printf("		");
// 		if (stack_b && diff <= 0)
// 		{
// 			ft_printf("	%d	%d", stack_b->value, stack_b->order);
// 			stack_b = stack_b->next;
// 			size_b--;
// 		}
// 		ft_printf("\n");
// 	}
// 	ft_printf("	-------------- --------------\n");
// 	ft_printf("	stack_a		stack_b\n");
// 	ft_printf("\n");
// }

int	find_min(t_stack *stack_a)
{
	int	min;

	if (!stack_a)
		return (0);
	min = stack_a->order;
	while (stack_a)
	{
		if (stack_a->order < min)
			min = stack_a->order;
		stack_a = stack_a->next;
	}
	return (min);
}

int	find_max(t_stack *stack_a)
{
	int	max;

	if (!stack_a)
		return (0);
	max = stack_a->order;
	while (stack_a)
	{
		if (stack_a->order > max)
			max = stack_a->order;
		stack_a = stack_a->next;
	}
	return (max);
}
