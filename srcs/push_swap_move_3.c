/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_move_3.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/20 17:19:29 by abinet            #+#    #+#             */
/*   Updated: 2023/03/20 17:19:47 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	reverse_rotate_rotate(t_stack **stack_a, t_stack **stack_b)
{
	t_stack	*begin;
	t_stack	*last;

	if (!(*stack_a)->next)
		return ;
	begin = *stack_a;
	while ((*stack_a)->next)
	{
		last = *stack_a;
		*stack_a = (*stack_a)->next;
	}
	last->next = NULL;
	(*stack_a)->next = begin;
	if (!(*stack_b)->next)
		return ;
	begin = *stack_b;
	while ((*stack_b)->next)
	{
		last = *stack_b;
		*stack_b = (*stack_b)->next;
	}
	last->next = NULL;
	(*stack_b)->next = begin;
	ft_printf("rrr\n");
}
