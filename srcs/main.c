/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/14 14:20:05 by abinet            #+#    #+#             */
/*   Updated: 2023/03/20 16:15:25 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	push_swap(t_stack **stack_a, t_stack **stack_b)
{
	int	size;

	size = ft_lstsize_t(*stack_a);
	if (checker_order(*stack_a) == 0)
		return ;
	if (size == 2)
		swap_a(stack_a);
	else if (size <= 5)
		algo_1(stack_a, stack_b, size);
	else
	{
		if (size <= 150)
			parsing_1(stack_a, stack_b, 10);
		else
			parsing_1(stack_a, stack_b, 25);
		back_to_a(stack_a, stack_b);
	}
}

int	main(int argc, char **argv)
{
	t_stack	*stack_a;
	t_stack	*stack_b;

	if (argc <= 0)
		return (ft_printf("Error\n"));
	if (argc == 1 || (argc == 2 && check_args(argv[1]) == 0))
		return (0);
	stack_a = NULL;
	stack_b = NULL;
	argc--;
	while (argc > 0)
	{
		if (check_args(argv[argc]) == 1)
			return (ft_lstclear_t(&stack_a), ft_printf("Error\n"), 0);
		ft_lstadd_front_t(&stack_a, ft_lstnew_t(ft_atoi(argv[argc])));
		argc--;
	}
	if (check_same_nbr(stack_a) == 1)
		return (ft_printf("Error\n"), ft_lstclear_t(&stack_a), 0);
	give_order(stack_a);
	push_swap(&stack_a, &stack_b);
	ft_lstclear_t(&stack_a);
	ft_lstclear_t(&stack_b);
	return (0);
}
