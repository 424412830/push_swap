/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_move_2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/20 11:12:21 by abinet            #+#    #+#             */
/*   Updated: 2023/03/20 17:19:55 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	rotate_a(t_stack **stack_a)
{
	t_stack	*begin;
	t_stack	*last;

	if (!*stack_a || !(*stack_a)->next)
		return ;
	begin = (*stack_a)->next;
	last = *stack_a;
	while ((*stack_a)->next)
		*stack_a = (*stack_a)->next;
	(*stack_a)->next = last;
	last->next = NULL;
	*stack_a = begin;
	ft_printf("ra\n");
}

void	rotate_b(t_stack **stack_b)
{
	t_stack	*begin;
	t_stack	*last;

	if (!*stack_b || !(*stack_b)->next)
		return ;
	begin = (*stack_b)->next;
	last = *stack_b;
	while ((*stack_b)->next)
		*stack_b = (*stack_b)->next;
	(*stack_b)->next = last;
	last->next = NULL;
	*stack_b = begin;
	ft_printf("rb\n");
}

void	rotate_rotate(t_stack **stack_a, t_stack **stack_b)
{
	t_stack	*begin;
	t_stack	*last;

	if (!*stack_a || !(*stack_a)->next)
		return ;
	begin = (*stack_a)->next;
	last = *stack_a;
	while ((*stack_a)->next)
		*stack_a = (*stack_a)->next;
	(*stack_a)->next = last;
	last->next = NULL;
	*stack_a = begin;
	if (!*stack_b || !(*stack_b)->next)
		return ;
	begin = (*stack_b)->next;
	last = *stack_b;
	while ((*stack_b)->next)
		*stack_b = (*stack_b)->next;
	(*stack_b)->next = last;
	last->next = NULL;
	*stack_b = begin;
	ft_printf("rr\n");
}

void	reverse_rotate_a(t_stack **stack_a)
{
	t_stack	*begin;
	t_stack	*last;

	if (!(*stack_a)->next)
		return ;
	begin = *stack_a;
	while ((*stack_a)->next)
	{
		last = *stack_a;
		*stack_a = (*stack_a)->next;
	}
	last->next = NULL;
	(*stack_a)->next = begin;
	ft_printf("rra\n");
}

void	reverse_rotate_b(t_stack **stack_b)
{
	t_stack	*begin;
	t_stack	*last;

	if (!(*stack_b)->next)
		return ;
	begin = *stack_b;
	while ((*stack_b)->next)
	{
		last = *stack_b;
		*stack_b = (*stack_b)->next;
	}
	last->next = NULL;
	(*stack_b)->next = begin;
	ft_printf("rrb\n");
}
