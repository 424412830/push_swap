/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_order.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/24 18:06:02 by abinet            #+#    #+#             */
/*   Updated: 2023/03/20 10:30:53 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

int	find_position(t_stack *stack_a, int order)
{
	int	i;

	i = 1;
	while (stack_a->order != order)
	{
		i++;
		stack_a = stack_a->next;
	}
	return (i);
}

void	give_order(t_stack *stack_a)
{
	int		*tab;
	int		i;
	t_stack	*begin;

	i = 0;
	begin = stack_a;
	tab = malloc(sizeof(int) * ft_lstsize_t(stack_a));
	while (stack_a)
	{
		tab[i] = stack_a->value;
		stack_a = stack_a->next;
		i++;
	}
	tab = tri_tab(tab, i);
	stack_a = begin;
	while (stack_a)
	{
		i = 0;
		while (tab[i] != stack_a->value)
			i++;
		stack_a->order = i + 1;
		stack_a = stack_a->next;
	}
	free(tab);
}

void	print_tab(int *tab, int nbr_elements)
{
	int	i;

	i = 0;
	while (i < nbr_elements)
	{
		ft_printf("%d ", tab[i]);
		i++;
	}
}

int	*tri_tab(int *tab, int nbr_elements)
{
	int		i;
	int		j;
	int		temp;

	i = 0;
	while (i < nbr_elements)
	{
		j = i + 1;
		while (j < nbr_elements)
		{
			if (tab[i] > tab[j])
			{
				temp = tab[i];
				tab[i] = tab[j];
				tab[j] = temp;
			}
			j++;
		}
		i++;
	}
	return (tab);
}
