/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/09 11:05:47 by abinet            #+#    #+#             */
/*   Updated: 2023/03/20 15:39:22 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	algo_1(t_stack **stack_a, t_stack **stack_b, int size)
{
	int	min;
	int	max;

	min = find_min(*stack_a);
	max = find_max(*stack_a);
	if (size == 3)
		tri_for_3(stack_a);
	else if (size == 4)
	{
		while ((*stack_a)->order != find_min(*stack_a))
			rotate_a(stack_a);
		push_b(stack_a, stack_b);
		algo_1(stack_a, stack_b, size - 1);
		push_a(stack_a, stack_b);
	}
	else if (size == 5)
	{
		while ((*stack_a)->order != min && (*stack_a)->order != max)
			rotate_a(stack_a);
		push_b(stack_a, stack_b);
		algo_1(stack_a, stack_b, size - 1);
		push_a(stack_a, stack_b);
		if ((*stack_a)->order != find_min(*stack_a))
			rotate_a(stack_a);
	}
}

void	tri_for_3(t_stack **stack_a)
{
	int	min;
	int	max;

	min = find_min(*stack_a);
	max = find_max(*stack_a);
	while (checker_order(*stack_a) != 0)
	{
		if ((*stack_a)->order == min)
			swap_a(stack_a);
		else if ((*stack_a)->order != max && (*stack_a)->next->order == min)
			swap_a(stack_a);
		else
			rotate_a(stack_a);
	}
}

void	parsing_1(t_stack **stack_a, t_stack **stack_b, int value_parsing)
{
	int	last;
	int	mid;
	int	i;

	i = 0;
	mid = ft_lstsize_t(*stack_a) / 2;
	while ((*stack_a)->next)
	{
		i = i + value_parsing;
		last = ft_lstlast_t(*stack_a)->order;
		while ((*stack_a)->order != last)
			parsing_2(stack_a, stack_b, mid, i);
	}
}

void	parsing_2(t_stack **stack_a, t_stack **stack_b, int mid, int i)
{
	if ((*stack_a)->order > (mid - i) && (*stack_a)->order < (mid + i))
	{
		push_b(stack_a, stack_b);
		if ((*stack_b)->next && (*stack_b)->order < mid)
		{
			if ((*stack_a)->order > (mid + i) || (*stack_a)->order < (mid - i))
				rotate_rotate(stack_a, stack_b);
			else
				rotate_b(stack_b);
		}
	}
	else
		rotate_a(stack_a);
}

void	back_to_a(t_stack **stack_a, t_stack **stack_b)
{
	int	size;
	int	max;

	while ((*stack_b))
	{
		size = ft_lstsize_t(*stack_b);
		max = find_max(*stack_b);
		while ((*stack_b)->order != max)
		{
			if (find_position(*stack_b, max) <= size / 2)
				rotate_b(stack_b);
			else
				reverse_rotate_b(stack_b);
		}
		push_a(stack_a, stack_b);
		if ((*stack_a)->next && (*stack_a)->order > (*stack_a)->next->order)
			swap_a(stack_a);
	}
}
