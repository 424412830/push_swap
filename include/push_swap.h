/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/30 14:20:11 by abinet            #+#    #+#             */
/*   Updated: 2023/03/20 15:37:47 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/libft.h"
# include <stdio.h>
# include <stdlib.h>
# include <limits.h>

typedef struct stack_list
{
	int					value;
	int					order;
	struct stack_list	*next;
}						t_stack;

t_stack	*ft_lstnew_t(int value);
void	ft_lstadd_front_t(t_stack **stack, t_stack *new);
void	ft_lstclear_t(t_stack **stack);
int		ft_lstsize_t(t_stack *stack);
t_stack	*ft_lstlast_t(t_stack *stack);
int		check_args(char	*str);
int		check_same_nbr(t_stack *stack);
int		checker_order(t_stack *stack);
int		checker_inverse_order(t_stack *stack);
int		find_min(t_stack *stack_a);
int		find_max(t_stack *stack_a);
int		*tri_tab(int *tab, int nbr_elements);
void	print_tab(int *tab, int nbr_elements);
void	give_order(t_stack *stack_a);
int		find_position(t_stack *stack_a, int order);
void	print_stack(t_stack *stack_a, t_stack *stack_b);
void	push_swap(t_stack **stack_a, t_stack **stack_b);
void	swap_a(t_stack **stack_a);
void	swap_b(t_stack **stack_b);
void	swap_swap(t_stack **stack_a, t_stack **stack_b);
void	push_a(t_stack **stack_a, t_stack **stack_b);
void	push_b(t_stack **stack_a, t_stack **stack_b);
void	rotate_a(t_stack **stack_a);
void	rotate_b(t_stack **stack_b);
void	rotate_rotate(t_stack **stack_a, t_stack **stack_b);
void	reverse_rotate_a(t_stack **stack_a);
void	reverse_rotate_b(t_stack **stack_b);
void	reverse_rotate_rotate(t_stack **stack_a, t_stack **stack_b);
void	algo_1(t_stack	**stack_a, t_stack **stack_b, int size);
void	parsing_1(t_stack **stack_a, t_stack **stack_b, int value_parsing);
void	parsing_2(t_stack **stack_a, t_stack **stack_b, int mid, int i);
void	back_to_a(t_stack **stack_a, t_stack **stack_b);
void	tri_for_3(t_stack **stack_a);

#endif
