NAME		= push_swap

################## FILES & DIRECTORY ###################################################

SRCS_FILES	= push_swap_move.c	push_swap_move_2.c	push_swap_utils.c	push_swap_lst.c	push_swap_checkers.c	main.c	algo.c	push_swap_order.c	push_swap_move_3.c
SRCS_DIR	= srcs/
SRCS		= $(addprefix $(SRCS_DIR),$(SRCS_FILES))


OBJS_FILES	= $(SRCS:.c=.o)
OBJS_DIR	= objs/
#OBJS		= $(addprefix $(OBJS_DIR),$(OBJS_FILES))

HEADER_DIR		= include/
HEADER_FILE		= push_swap.h
HEADER			= $(addprefix ${HEADER_DIR}, ${HEADER_FILE})

LIBFT		= libft/libft.a
LIBFT_PATH	= libft/

########################################################################################

CC			= cc

FLAGS		= -Wall -Wextra -Werror -g3

RM			= rm -rf


###################### RULES ##########################################################


%.o: %.c
		${CC} ${FLAGS} -c $< -o ${<:.c=.o}

${NAME}:	${OBJS_FILES} ${HEADER} ${LIBFT}
		${CC} ${OBJS_FILES} ${LIBFT} -o ${NAME}
		mkdir -p ${OBJS_DIR}
		mv ${OBJS_FILES} ${OBJS_DIR}

${LIBFT}:
		make -C ${LIBFT_PATH}

all:	${NAME}

clean :
		make -C ${LIBFT_PATH} clean
		${RM} ${OBJS_DIR}

fclean : clean
		make -C ${LIBFT_PATH} fclean
		${RM} ${NAME}

re : fclean all

.SECONDARY : $(OBJS_FILES)

.PHONY : all clean fclean re bonus so
